package com.glearning.empcrud.service;

import com.glearning.empcrud.model.User;
import java.util.List;
import java.util.Set;

public interface UserService {

    User saveUser(User user);


    List<User> getAllUsers();
}