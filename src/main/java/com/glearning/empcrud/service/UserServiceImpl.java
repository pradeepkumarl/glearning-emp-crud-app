package com.glearning.empcrud.service;

import com.glearning.empcrud.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private List<User> users = new ArrayList<>();

    @Override
    public User saveUser(User user) {
        this.users.add(user);
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return this.users;
    }
}